# JSON Web Key (JWK) set loader

JWK set loader for servlet-based applications. Supports PKCS#11 for Hardware 
Security Modules (HSM) and smart cards.

Used by the Connect2id server to sign tokens with RSA and EC keys stored in an
HSM.

Requires Java 17+.


## Change log

* version 1.0 (2017-01-19)
    * Initial release.

* version 1.1 (2017-01-20)
    * Factors out JWKSetLoader.loadPKCS11Keys.

* version 1.2 (2017-01-22)
    * Logs JWK meta details after JWK set loading.
    * Renames package to com.nimbusds.jose.jwk.loader.
    * Renames artifactId to nimbus-jwkset-loader.

* version 1.2.1 (2017-01-27)
    * Fixes bug which prevented loading PKCS#11 keys with their use set in the 
      X.509 certificate to signature (digital signature, non-repudiation).

* version 1.2.2 (2017-01-27)
    * Refines logging on PKCS#11 JWK loading.

* version 1.3 (2017-02-15)
    * Configuration properties may be overridden by system properties with 
      matching key (issue #1).

* version 1.4 (2017-11-28)
    * The JWK set can be alternatively passed via a "jose.jwkSet" system 
      property, overriding "/WEB-INF/jwkSet.json". The JWK set can be passed in 
      the standard JSON format or with additional BASE64URL encoding to prevent 
      escaping issues in the command line shell.
      
* version 1.5 (2018-03-06)
    * Adds new JWKSetLoader.loadJWKSetFromProperty(Properties) method. 

* version 2.0 (2018-09-21)
    * Refactors JWKSetLoader, removes Servlet dependency.
    * Bumps Nimbus JOSE+JWT dependency to 6.0.  

* version 3.0 (2018-09-21)
    * Updates JWKSetLoader to support multi-tenant use.

* version 3.1 (2018-09-22)
    * Makes the JWKSetLoader.loadFromInputStream, 
      JWKSetLoader.loadFromSystemProperty and JWKSetLoader.loadPKCS11Keys 
      methods public.
      
* version 4.0 (2019-04-25)
    * Upgrades to Java 11 (including new SunPKCS11 security provider). 

* version 4.1 (2019-07-08)
    * Don't clear the jose.jwkSet Java system property after loading to allow 
      subsequent loads from client code.
      
* version 5.0 (2020-06-01)
    * Adds support for custom JWK set file name and property name.

* version 5.1 (2021-04-14)
    * Bumps dependencies.

* version 5.2 (2021-10-20)
    * Updates the SE3000 log line to show when an X.509 certificate is present.

* version 5.2.1 (2022-03-22)
    * Bumps Nimbus JOSE+JWT dependency to 9.21.

* version 5.2.2 (2023-02-03)
    * SigningJWKFeeder must bias key selection towards the most future 
      expiring, but still active (issue #5)
    * Fixes SE2000 error log message (issue #4)
    * Extends and updates tests.
    * Bumps Nimbus JOSE+JWT dependency to 9.30.1.  

* version 5.3 (2023-10-07)
    * Adds an optional pkcs11.keyIDs.* configuration property specifying the 
      IDs of PKCS#11 keys to load. If left unspecified all PKCS#11 keys will be 
      loaded.
    * Adds com.thetransactioncompany:java-property-utils:1.17 dependency. 
    * Bumps Nimbus JOSE+JWT dependency to 9.36.
    * Bumps Log4j dependency to 2.20.0.

* version 6.0 (2023-12-14)
    * Java 17.
    * Bumps Nimbus JOSE+JWT dependency to 9.37.3.
    * Bumps BouncyCastle to 1.77.

* version 7.0 (2024-11-26)
    * Removes the optional jose.allowWeakKeys configuration property, all 
      loaded RSA keys must be at least 2048 bits long.
    * Factors out JWKSetLoader.loadFromPKCS11(FileInputStreamSource).
    * Adds WeakRSAKeyDetector.ensureNoWeakRSAKeys method.
    * Makes JWKMetaLogger class public.
    * Bumps Nimbus JOSE+JWT dependency to 9.47.
    * Bumps Log4j dependency to 2.24.2.
    * Bumps BouncyCastle to 1.78.1.

* version 7.1 (2024-11-30)
    * JWKMetaLogger.log(JWKSet) logs JWK thumbprint (RFC 7638).
    * General private key detection in JWKMetaLogger.log(JWKSet).

* version 7.2 (2024-12-01)
    * Exposes PKCS11Configuration class.
    * Adds static JWKSetLoader.loadFromPKCS11(PKCS11Configuration, 
      FileInputStreamSource) method.

* version 7.3 (2024-12-02)
    * Makes PKCS11Configuration method access public.
    * Optimises JWKSetMerge.merge.

* version 7.3.1 (2025-01-20)
    * Bumps Nimbus JOSE+JWT to 10.0.1.
    * Bumps BouncyCastle to 1.79.
