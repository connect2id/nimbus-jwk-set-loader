package com.nimbusds.jose.jwk.loader;


import java.io.InputStream;


/**
 * File input stream source.
 */
public interface FileInputStreamSource {
	
	
	/**
	 * Returns an input stream for the specified file.
	 *
	 * @param filename The file name. Must not be {@code null}.
	 *
	 * @return The input stream, {@code null} if the file doesn't exist or
	 *         couldn't be accessed.
	 */
	InputStream getInputSteam(final String filename);
}
