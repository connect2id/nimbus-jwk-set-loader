package com.nimbusds.jose.jwk.loader;


import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.AsymmetricJWK;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.jose.util.X509CertUtils;

import java.security.cert.X509Certificate;
import java.time.format.DateTimeFormatter;
import java.util.Date;


/**
 * JSON Web Key (JWK) meta parameter logger.
 */
public class JWKMetaLogger {
	
	
	/**
	 * Logs the meta parameters of the keys in the JWK set at INFO level.
	 *
	 * @param jwkSet The JWK set. Must not be {@code null}.
	 */
	public static void log(final JWKSet jwkSet) {
		
		Loggers.MAIN_LOG.info("[SE1008] JWK set:");
		for (int i = 0; i < jwkSet.getKeys().size(); i++) {
			JWKMetaLogger.log(i + 1, jwkSet.getKeys().get(i));
		}
	}
	
	
	/**
	 * Logs the JWK meta parameters at INFO level.
	 *
	 * @param pos The JWK position (index).
	 * @param jwk The JWK, {@code null} if not specified.
	 */
	private static void log(final int pos, final JWK jwk) {
		
		if (jwk == null) {
			return;
		}
		
		String isPrivate = "";
		
		if (jwk instanceof AsymmetricJWK) {
			isPrivate = jwk.isPrivate() ? " private=true" : " private=false";
		}
		
		String validityTimeWindow = "";
		
		final boolean x509CertPresent =
			jwk.getX509CertChain() != null
			&& ! jwk.getX509CertChain().isEmpty()
			&& jwk.getX509CertChain().get(0) != null;
		
		if (x509CertPresent) {
			
			byte[] derCert = jwk.getX509CertChain().get(0).decode();
			
			X509Certificate cert = X509CertUtils.parse(derCert);
			
			if (cert != null) {
				Date nbf = cert.getNotBefore();
				
				if (nbf != null) {
					validityTimeWindow = "nbf=" +
						DateTimeFormatter.ISO_INSTANT.format(nbf.toInstant());
				}
				
				Date exp = cert.getNotAfter();
				
				if (exp != null) {
					validityTimeWindow += " exp=" +
						DateTimeFormatter.ISO_INSTANT.format(exp.toInstant());
				}
			}
		}

		Base64URL thumbprint;
                try {
                        thumbprint = jwk.computeThumbprint();
                } catch (JOSEException e) {
                        throw new RuntimeException(e);
                }

                Loggers.MAIN_LOG.info("[SE3000] [{}] JWK type={} id={}{} use={} size={} x5c={} pkcs#11={} thumbprint={} {}",
			pos,
			jwk.getKeyType(),
			jwk.getKeyID(),
			isPrivate,
			jwk.getKeyUse(),
			jwk.size(),
			x509CertPresent,
			PKCS11Utils.isPKCS11Key(jwk),
			thumbprint,
			validityTimeWindow
			);
	}
	
	
	/**
	 * Prevents public instantiation.
	 */
	private JWKMetaLogger() {}
}
