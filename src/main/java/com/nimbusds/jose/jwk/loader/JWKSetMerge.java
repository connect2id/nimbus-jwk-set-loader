package com.nimbusds.jose.jwk.loader;


import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;


/**
 * JWK set merge routine.
 */
public class JWKSetMerge {
	
	
	/**
	 * Merges the specified JWK sets by prefixing the keys from the
	 * PKCS#11 set.
	 *
	 * @param plainJWKSet  The plain JWK set. Must not be {@code null}.
	 * @param pkcs11JWKSet The PKCS#11 JWK set, {@code null} if none.
	 *
	 * @return The JWK set.
	 */
	public static JWKSet merge(final JWKSet plainJWKSet, final JWKSet pkcs11JWKSet) {

		if (pkcs11JWKSet == null) {
			return Objects.requireNonNull(plainJWKSet);
		}

		// Prepend the HSM keys
		List<JWK> keys = new LinkedList<>(pkcs11JWKSet.getKeys());
                keys.addAll(plainJWKSet.getKeys());
		return new JWKSet(keys);
	}
	
	
	/**
	 * Prevents public instantiation.
	 */
	private JWKSetMerge() {}
}
