package com.nimbusds.jose.jwk.loader;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Loggers.
 */
class Loggers {
	
	/**
	 * The main logger.
	 */
	public static final Logger MAIN_LOG = LogManager.getLogger("MAIN");
}
