package com.nimbusds.jose.jwk.loader;


import java.security.cert.X509Certificate;
import java.util.*;

import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.KeyType;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.util.Base64;
import com.nimbusds.jose.util.X509CertUtils;


/**
 * Feeder of JSON Web Keys (JWK) for digital signing.
 */
public class SigningJWKFeeder<T extends JWK> {
	
	
	/**
	 * JWKs with X.509 certificate, sorted by its expiration date (from
	 * future to past).
	 */
	protected final HashMap<X509Certificate,T> sortedX509Keys = new LinkedHashMap<>();
	
	
	/**
	 * Plain JWKs in their original order.
	 */
	protected final List<T> plainKeys = new LinkedList<>();
	
	
	private static X509Certificate getCertificate(final JWK jwk) {
		
		if (jwk.getX509CertChain() == null || jwk.getX509CertChain().isEmpty()) {
			return null;
		}
		
		Base64 certBytes = jwk.getX509CertChain().get(0);
		
		if (certBytes == null) {
			throw new IllegalArgumentException("Couldn't parse x.509 certificate from JWK with ID " + jwk.getKeyID() + ": Empty certificate");
		}
		
		X509Certificate cert = X509CertUtils.parse(certBytes.decode());
		
		if (cert == null) {
			throw new IllegalArgumentException("Couldn't parse X.509 certificate from JWK with ID " + jwk.getKeyID());
		}
		
		// Cert must specify a validity time window
		if (cert.getNotBefore() == null) {
			throw new IllegalArgumentException("Missing not-before attribute for X.509 certificate for JWK with ID " + jwk.getKeyID());
		}
		if (cert.getNotAfter() == null) {
			throw new IllegalArgumentException("Missing not-after attribute for X.509 certificate for JWK with ID " + jwk.getKeyID());
		}
		
		return cert;
	}
	
	
	/**
	 * Creates a new feeder for signing JWKs.
	 *
	 * @param jwkList The list of JWKs with use signature and private part.
	 *                Must not be empty or {@code null}.
	 */
	public SigningJWKFeeder(final List<T> jwkList) {
		
		if (jwkList.isEmpty()) {
			throw new IllegalArgumentException("The JWK list must not be empty");
		}
		
		List<T> x509Keys = new LinkedList<>();
		
		for (T jwk: jwkList) {
			
			if (! KeyUse.SIGNATURE.equals(jwk.getKeyUse())) {
				throw new IllegalArgumentException("The use of JWK with ID " + jwk.getKeyID() + " must be signature");
			}

			if (! jwk.isPrivate()) {
				throw new IllegalArgumentException("The JWK with ID " + jwk.getKeyID() + " has no private part");
			}
			
			if (getCertificate(jwk) != null) {
				// X.509 cert / PKCS#11 based key
				x509Keys.add(jwk);
			} else {
				// Plain key
				plainKeys.add(jwk);
			}
		}
		
		x509Keys.sort(Collections.reverseOrder(new X509CertExpirationComparator()));
		
		for (T jwk: x509Keys) {
			sortedX509Keys.put(getCertificate(jwk), jwk);
		}
	}
	
	
	/**
	 * Gets the JWK to use for signing.
	 *
	 * @return The JWK to use for signing.
	 */
	public T getJWK() {
		
		if (! sortedX509Keys.isEmpty()) {
			
			// Get first key that is valid according to the time window of its certificate
			
			final var now = new Date();
			
			KeyType keyType = null;
			
			for (Map.Entry<X509Certificate,T> entry: sortedX509Keys.entrySet()) {
				
				X509Certificate cert = entry.getKey();
				
				if (now.before(cert.getNotBefore())) {
					continue; // skip, cannot be used yet
				}
				
				if (now.before(cert.getNotAfter())) {
					return entry.getValue(); // okay, not expired
				}
				
				keyType = entry.getValue().getKeyType();
			}
			
			Loggers.MAIN_LOG.error("[SE2000] No signing {} key with active (nbf < now < exp) X.509 certificate, " +
				"using the first available. Add new key(s)!", keyType);
			
			// Use first available key
			return sortedX509Keys.values().iterator().next();
			
		} else {
			// Return the first available plain JWK
			return plainKeys.get(0);
		}
	}
	
	
	/**
	 * Returns the total number JWKs in this feeder.
	 *
	 * @return The total number of JWKs.
	 */
	public int size() {
		
		return sortedX509Keys.size() + plainKeys.size();
	}
}
