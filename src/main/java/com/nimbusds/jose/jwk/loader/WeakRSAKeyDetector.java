package com.nimbusds.jose.jwk.loader;


import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;

import java.util.LinkedList;
import java.util.List;


/**
 * Detector of weak RSA keys (shorter than 2048 bits).
 */
public class WeakRSAKeyDetector {
	
	
	/**
	 * Finds weak RSA keys (shorter than 2048 bits) in the specified JWK
	 * set.
	 *
	 * @param jwkSet The JWK set.
	 *
	 * @return The key IDs of the weak keys, empty list if none found. May
	 *         contain null IDs.
	 */
	public static List<String> findWeakRSAKeys(final JWKSet jwkSet) {
		
		List<String> idsOfWeakKeys = new LinkedList<>();
		
		jwkSet.getKeys().forEach(jwk -> {
			if (jwk instanceof RSAKey rsaJWK) {
                                if (rsaJWK.size() < 2048) {
					idsOfWeakKeys.add(rsaJWK.getKeyID());
				}
			}
		});
		
		return idsOfWeakKeys;
	}


	/**
	 * Ensures there are no weak RSA keys (shorter than 2048 bits) in the
	 * specified JWK set.
	 *
	 * @param jwkSet The JWK set.
	 *
	 * @throws JOSEException If one or more weak RSA keys are found.
	 */
	public static void ensureNoWeakRSAKeys(final JWKSet jwkSet)
		throws JOSEException {

		List<String> idsOfWeakKeys = WeakRSAKeyDetector.findWeakRSAKeys(jwkSet);

		if (idsOfWeakKeys.isEmpty()) {
			return;
		}

		String msg = "Found weak RSA key(s) shorter than 2048 bits with IDs: " + idsOfWeakKeys;
		Loggers.MAIN_LOG.error("[SE1030] {}", msg);
		throw new JOSEException(msg);
	}
}
