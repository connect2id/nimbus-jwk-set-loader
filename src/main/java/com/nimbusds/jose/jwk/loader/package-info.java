/**
 * Utilities for Javascript Object Signing and Encryption (JOSE) with PKCS#11
 * for Hardware Security Modules (HSM) and smart cards.
 */
package com.nimbusds.jose.jwk.loader;