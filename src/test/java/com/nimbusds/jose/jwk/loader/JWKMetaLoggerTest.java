package com.nimbusds.jose.jwk.loader;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import org.junit.Test;

public class JWKMetaLoggerTest {


        @Test
        public void logMinimal() throws JOSEException {

                JWK jwk = new RSAKeyGenerator(2048)
                        .generate();

                JWKMetaLogger.log(new JWKSet(jwk));
        }
}