package com.nimbusds.jose.jwk.loader;


import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.*;
import com.nimbusds.jose.jwk.gen.ECKeyGenerator;
import com.nimbusds.jose.jwk.gen.OctetSequenceKeyGenerator;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import com.nimbusds.jose.util.Base64;
import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.jose.util.IOUtils;
import com.nimbusds.jose.util.X509CertUtils;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

import static org.junit.Assert.*;


public class JWKSetLoaderTest {
	
	
	private static String HSM_CONFIG;
	
	
	private static String HSM_PIN;
	
	
	private static final JWKSet FILE_BASED_JWK_SET;
	
	
	static {
		try {
			HSM_CONFIG = IOUtils.readFileToString(new File("test-hsm.cfg"), StandardCharsets.UTF_8);
			HSM_PIN = IOUtils.readFileToString(new File("test-hsm-pin.txt"), StandardCharsets.UTF_8);
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		List<JWK> keys = new LinkedList<>();
		keys.add(generateRSAJWK("1"));
		keys.add(generateECKey("2"));
		keys.add(generateAESKey("hmac"));
		keys.add(generateAESKey("subject-encrypt"));
		FILE_BASED_JWK_SET = new JWKSet(keys);
		assertEquals(4, FILE_BASED_JWK_SET.size());
	}
	
	
	private KeyStore hsmKeyStore;
	
	
	@Before
	public void setUp()
		throws Exception {
		
		Provider hsmProvider = loadHSMProvider(HSM_CONFIG);
		
		hsmKeyStore = loadHSMKeyStore(hsmProvider, HSM_PIN);
		
		assertEquals("PKCS11", hsmKeyStore.getType());
		
		// Clear keys
		for (Enumeration<String> keyIds = hsmKeyStore.aliases(); keyIds.hasMoreElements(); ) {
			hsmKeyStore.deleteEntry(keyIds.nextElement());
		}
	}
	
	
	@After
	public void tearDown() {
		
		System.clearProperty("jose.jwkSet");
		System.clearProperty("pkcs11.enable");
		System.clearProperty("pkcs11.configFile");
		System.clearProperty("pkcs11.password");
		System.clearProperty("pkcs11.keyIDs.1");
		System.clearProperty("pkcs11.keyIDs.2");
		System.clearProperty("pkcs11.keyIDs.3");

		System.clearProperty("keyStore.pkcs11.op.enable");
		System.clearProperty("keyStore.pkcs11.op.configFile");
		System.clearProperty("keyStore.pkcs11.op.password");
		System.clearProperty("keyStore.pkcs11.op.keyIDs.1");
		System.clearProperty("keyStore.pkcs11.op.keyIDs.2");
		System.clearProperty("keyStore.pkcs11.op.keyIDs.3");
	}
	
	
	private static RSAKey generateRSAJWK(final String kid) {
		
		try {
			return new RSAKeyGenerator(2048)
				.keyID(kid)
				.keyUse(KeyUse.SIGNATURE)
				.generate();
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	private static ECKey generateECKey(final String kid) {
		
		try {
			return new ECKeyGenerator(Curve.P_256)
				.keyID(kid)
				.keyUse(KeyUse.SIGNATURE)
				.generate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	private static OctetSequenceKey generateAESKey(final String kid) {
		
		try {
			return new OctetSequenceKeyGenerator(128)
				.keyID(kid)
				.keyUse(KeyUse.ENCRYPTION)
				.generate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	private static Provider loadHSMProvider(final String hsmConfig) {
		Provider hsmProvider = Security.getProvider("SunPKCS11");
		return hsmProvider.configure("--" + hsmConfig);
	}
	
	
	private static KeyStore loadHSMKeyStore(final Provider hsmProvider, final String userPin)
		throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
		
		KeyStore keyStore = KeyStore.getInstance("PKCS11", hsmProvider);
		keyStore.load(null, userPin.toCharArray());
		return keyStore;
	}
	
	
	private static void generateRSAKeyWithSelfSignedCert(final KeyStore hsmKeyStore, final String kid) {
		
		generateRSAKeyWithSelfSignedCert(hsmKeyStore, kid, null);
	}
	
	
	private static void generateRSAKeyWithSelfSignedCert(final KeyStore hsmKeyStore,
							     final String kid,
							     final KeyUse keyUse) {
		
		generateRSAKeyWithSelfSignedCert(hsmKeyStore, 2048, kid, keyUse);
	}


	private static void generateRSAKeyWithSelfSignedCert(final KeyStore hsmKeyStore,
							     final int keySize,
							     final String kid,
							     final KeyUse keyUse) {

		try {
			KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA", hsmKeyStore.getProvider());
			keyPairGenerator.initialize(keySize);
			KeyPair keyPair = keyPairGenerator.generateKeyPair();

			var issuer = new X500Name("cn=c2id");
			var serialNumber = new BigInteger(64, new SecureRandom());
			var now = new Date();
			var nbf = new Date(now.getTime() - 1000L);
			var exp = new Date(now.getTime() + 365 * 24 * 60 * 60 * 1000L); // in 1 year
			var subject = new X500Name("cn=c2id");
			var x509certBuilder = new JcaX509v3CertificateBuilder(
				issuer,
				serialNumber,
				nbf,
				exp,
				subject,
				keyPair.getPublic()
			);

			if (KeyUse.SIGNATURE.equals(keyUse)) {
				KeyUsage keyUsage = new KeyUsage(KeyUsage.nonRepudiation | KeyUsage.digitalSignature);
				x509certBuilder.addExtension(Extension.keyUsage, true, keyUsage);
			} else if (KeyUse.ENCRYPTION.equals(keyUse)) {
				KeyUsage keyUsage = new KeyUsage(KeyUsage.dataEncipherment);
				x509certBuilder.addExtension(Extension.keyUsage, true, keyUsage);
			}

			var signerBuilder = new JcaContentSignerBuilder("SHA256withRSA");
			signerBuilder.setProvider(hsmKeyStore.getProvider());
			X509CertificateHolder certHolder = x509certBuilder.build(signerBuilder.build(keyPair.getPrivate()));
			X509Certificate cert = X509CertUtils.parse(certHolder.getEncoded());
			assertNotNull(cert);

			hsmKeyStore.setKeyEntry(kid, keyPair.getPrivate(), "".toCharArray(), new java.security.cert.Certificate[]{cert});

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	

	@Test
	public void testConstants() {
		
		assertEquals("jose.jwkSet", JWKSetLoader.DEFAULT_JWK_SET_PROPERTY_NAME);
		
		assertEquals("/WEB-INF/jwkSet.json", JWKSetLoader.DEFAULT_JWK_SET_FILENAME);
	}
	
	
	@Test
	public void loadFromDefaultProperty_ok()
		throws JOSEException {
		
		var jwkSet = new JWKSet(generateRSAJWK("1"));
		
		var props = new Properties();
		props.setProperty(JWKSetLoader.DEFAULT_JWK_SET_PROPERTY_NAME, jwkSet.toString(false));
		
		JWKSet out = JWKSetLoader.loadFromDefaultProperty(props);
		
		assertEquals(jwkSet.getKeyByKeyId("1").computeThumbprint(), out.getKeyByKeyId("1").computeThumbprint());
		
		assertEquals(1, out.getKeys().size());
	}


	@Test
	public void loadFromDefaultSystemProperty_ok()
		throws JOSEException {

		var jwkSet = new JWKSet(generateRSAJWK("1"));

		System.setProperty(JWKSetLoader.DEFAULT_JWK_SET_PROPERTY_NAME, jwkSet.toString(false));

		JWKSet out = JWKSetLoader.loadFromDefaultSystemProperty();

		assertEquals(jwkSet.getKeyByKeyId("1").computeThumbprint(), out.getKeyByKeyId("1").computeThumbprint());

		assertEquals(1, out.getKeys().size());
	}
	
	
	@Test
	public void loadFromDefaultProperty_base64URLEncoded_ok()
		throws JOSEException {
		
		var jwkSet = new JWKSet(generateRSAJWK("1"));
		
		var props = new Properties();
		props.setProperty(JWKSetLoader.DEFAULT_JWK_SET_PROPERTY_NAME, Base64URL.encode(jwkSet.toString(false)).toString());
		
		JWKSet out = JWKSetLoader.loadFromDefaultProperty(props);
		
		assertEquals(jwkSet.getKeyByKeyId("1").computeThumbprint(), out.getKeyByKeyId("1").computeThumbprint());
		
		assertEquals(1, out.getKeys().size());
	}


	@Test
	public void loadFromDefaultSystemProperty_base64URLEncoded_ok()
		throws JOSEException {

		var jwkSet = new JWKSet(generateRSAJWK("1"));

		System.setProperty(JWKSetLoader.DEFAULT_JWK_SET_PROPERTY_NAME, Base64URL.encode(jwkSet.toString(false)).toString());

		JWKSet out = JWKSetLoader.loadFromDefaultSystemProperty();

		assertEquals(jwkSet.getKeyByKeyId("1").computeThumbprint(), out.getKeyByKeyId("1").computeThumbprint());

		assertEquals(1, out.getKeys().size());
	}
	
	
	@Test
	public void loadFromDefaultProperty_none()
		throws JOSEException {
		
		assertNull(JWKSetLoader.loadFromDefaultProperty(new Properties()));
	}


	@Test
	public void loadFromDefaultSystemProperty_none()
		throws JOSEException {

		assertNull(JWKSetLoader.loadFromDefaultSystemProperty());
	}


	@Test
	public void loadFromDefaultProperty_rejectWeakRSAKeys()
		throws JOSEException {

		var jwkSet = new JWKSet(
			new RSAKeyGenerator(1024, true)
				.keyID("1")
				.keyUse(KeyUse.SIGNATURE)
				.generate()
		);

		var props = new Properties();
		props.setProperty(JWKSetLoader.DEFAULT_JWK_SET_PROPERTY_NAME, jwkSet.toString(false));

		try {
			JWKSetLoader.loadFromDefaultProperty(props);
			fail();
		} catch (JOSEException e) {
			assertEquals("Found weak RSA key(s) shorter than 2048 bits with IDs: [1]", e.getMessage());
		}
	}


	@Test
	public void loadFromDefaultSystemProperty_rejectWeakRSAKeys()
		throws JOSEException {

		var jwkSet = new JWKSet(
			new RSAKeyGenerator(1024, true)
				.keyID("1")
				.keyUse(KeyUse.SIGNATURE)
				.generate()
		);

		System.setProperty(JWKSetLoader.DEFAULT_JWK_SET_PROPERTY_NAME, jwkSet.toString(false));

		try {
			JWKSetLoader.loadFromDefaultSystemProperty();
			fail();
		} catch (JOSEException e) {
			assertEquals("Found weak RSA key(s) shorter than 2048 bits with IDs: [1]", e.getMessage());
		}
	}
	
	
	@Test
	public void loadWithPublicJWKForHistoricalPurposes() throws JOSEException {
		
		var in = new JWKSet(
			List.of(
				new RSAKeyGenerator(2048, true).keyID("s2").generate(),
				new RSAKeyGenerator(2048, true).keyID("s1").generate().toPublicJWK()
			)
		);

		System.setProperty("jose.jwkSet", in.toString(false));
		
		JWKSet out = JWKSetLoader.loadWithDefaultSystemPropertyOverrideAndPKCS11Support(filename -> null);
		
		assertEquals("s2", out.getKeys().get(0).getKeyID());
		assertTrue(out.getKeys().get(0).isPrivate());

		assertEquals("s1", out.getKeys().get(1).getKeyID());
		assertFalse(out.getKeys().get(1).isPrivate());

		assertEquals(2, out.size());
	}
	
	
	@Test
	public void loadFromDefaultProperty_invalidJWKSet() {
		
		var props = new Properties();
		props.setProperty(JWKSetLoader.DEFAULT_JWK_SET_PROPERTY_NAME, "{}");
		
		try {
			JWKSetLoader.loadFromDefaultProperty(props);
			fail();
		} catch (JOSEException e) {
			assertEquals("Invalid JWK set: Missing required \"keys\" member", e.getMessage());
		}
	}
	
	
	@Test
	public void loadFromDefaultProperty_base64URLEncoded_invalidJWKSet() {
		
		var props = new Properties();
		props.setProperty(JWKSetLoader.DEFAULT_JWK_SET_PROPERTY_NAME, Base64URL.encode("{}").toString());
		
		try {
			JWKSetLoader.loadFromDefaultProperty(props);
			fail();
		} catch (JOSEException e) {
			assertEquals("Invalid JWK set: Missing required \"keys\" member", e.getMessage());
		}
	}


	@Test
	public void loadFromHSM_keyIDs_sysPropOverride()
		throws Exception {

		String kidHSM_1 = UUID.randomUUID().toString();
		String kidHSM_2 = UUID.randomUUID().toString();

		generateRSAKeyWithSelfSignedCert(hsmKeyStore, kidHSM_1, KeyUse.SIGNATURE);
		generateRSAKeyWithSelfSignedCert(hsmKeyStore, kidHSM_2, KeyUse.SIGNATURE);

		FileInputStreamSource fisSources = filename -> null;

		for (boolean base64EncodeConfig: List.of(false, true)) {

			System.setProperty("keyStore.pkcs11.op.enable", "true");
			System.setProperty("keyStore.pkcs11.op.configFile", base64EncodeConfig ? Base64.encode(HSM_CONFIG).toString() : HSM_CONFIG);
			System.setProperty("keyStore.pkcs11.op.password", HSM_PIN);
			System.setProperty("keyStore.pkcs11.op.keyIDs.1", kidHSM_1);

			var config = new PKCS11Configuration("keyStore.pkcs11.op.", new Properties(), true);
			assertTrue(config.isPKCS11Enabled());
			assertEquals(base64EncodeConfig ? Base64.encode(HSM_CONFIG).toString() : HSM_CONFIG, config.getPKCS11ConfigurationFile());
			assertEquals(HSM_PIN, new String(config.getPKCS11KeyStorePassword()));
			assertEquals(List.of(kidHSM_1), config.getPKCS11KeyIDs());

			JWKSet jwkSet = JWKSetLoader.loadFromPKCS11(config, fisSources);

			assertEquals(1, jwkSet.size());

			// Get RSA key
			var rsaJWK = (RSAKey) jwkSet.getKeyByKeyId(kidHSM_1);
			assertTrue(rsaJWK.isPrivate());
			assertEquals(kidHSM_1, rsaJWK.getKeyID());
			assertEquals(KeyUse.SIGNATURE, rsaJWK.getKeyUse());
			assertEquals("SunPKCS11-NitroKeyHSM", rsaJWK.getKeyStore().getProvider().getName());
			assertTrue(PKCS11Utils.isPKCS11Key(rsaJWK));
			assertTrue(PKCS11Utils.hasPKCS11Key(jwkSet));

			// Test RSA sign and verify
			var jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.RS256), new Payload("Hello, world!"));
			JWSSigner signer = new RSASSASigner(rsaJWK.toPrivateKey());
			signer.getJCAContext().setProvider(rsaJWK.getKeyStore().getProvider());
			jwsObject.sign(signer);
			assertTrue(jwsObject.verify(new RSASSAVerifier(rsaJWK)));
		}
	}
	
	
	@Test
	public void loadWithSysPropOverrideAndHSM_sysPropParseException() {
		
		System.setProperty(JWKSetLoader.DEFAULT_JWK_SET_PROPERTY_NAME, "invalid-key");
		
		try {
			JWKSetLoader.loadWithDefaultSystemPropertyOverrideAndPKCS11Support(filename -> null);
			fail();
		} catch (RuntimeException e) {
			assertTrue(e.getMessage().startsWith("Couldn't load JWK set from system property: jose.jwkSet: Invalid JWK set:"));
		}
	}


	@Test
	public void loadWithSysPropOverrideAndHSM_rejectWeakRSAKeys() throws JOSEException {

		var in = new JWKSet(new RSAKeyGenerator(1024, true).keyID("s1").generate());

		System.setProperty("jose.jwkSet", in.toString(false));

		try {
			JWKSetLoader.loadWithDefaultSystemPropertyOverrideAndPKCS11Support(filename -> null);
			fail();
		} catch (RuntimeException e) {
			assertEquals("Couldn't load JWK set from system property: jose.jwkSet: Found weak RSA key(s) shorter than 2048 bits with IDs: [s1]", e.getMessage());
		}
	}
	
	
	@Test
	public void loadWithSysPropOverrideAndHSM_jwkSetProperty_hsmConfigNotPresent() {
		
		System.setProperty(JWKSetLoader.DEFAULT_JWK_SET_PROPERTY_NAME, FILE_BASED_JWK_SET.toString(false));
		
		JWKSet jwkSet = JWKSetLoader.loadWithDefaultSystemPropertyOverrideAndPKCS11Support(filename -> null);
		
		assertNotNull("Property must not be cleared", System.getProperty(JWKSetLoader.DEFAULT_JWK_SET_PROPERTY_NAME));
		
		assertEquals("1", jwkSet.getKeys().get(0).getKeyID());
		assertEquals("2", jwkSet.getKeys().get(1).getKeyID());
		assertEquals("hmac", jwkSet.getKeys().get(2).getKeyID());
		assertEquals("subject-encrypt", jwkSet.getKeys().get(3).getKeyID());
		assertEquals(4, jwkSet.getKeys().size());
	}
	
	
	@Test
	public void loadWithSysPropOverrideAndHSM_jwkSetProperty_base64URLEncoded_hsmConfigNotPresent() {
		
		System.setProperty(
			JWKSetLoader.DEFAULT_JWK_SET_PROPERTY_NAME,
			Base64URL.encode(FILE_BASED_JWK_SET.toString(false)).toString()
		);
		
		JWKSet jwkSet = JWKSetLoader.loadWithDefaultSystemPropertyOverrideAndPKCS11Support(filename -> null);
		
		assertNotNull("Property must not be cleared", System.getProperty(JWKSetLoader.DEFAULT_JWK_SET_PROPERTY_NAME));
		
		assertEquals("1", jwkSet.getKeys().get(0).getKeyID());
		assertEquals("2", jwkSet.getKeys().get(1).getKeyID());
		assertEquals("hmac", jwkSet.getKeys().get(2).getKeyID());
		assertEquals("subject-encrypt", jwkSet.getKeys().get(3).getKeyID());
		assertEquals(4, jwkSet.getKeys().size());
	}
	
	
	@Test
	public void loadWithSysPropOverrideAndHSM_jwkSetFile_hsmConfigNotPresent() {
		
		FileInputStreamSource fs = filename -> {
			if (PKCS11Configuration.DEFAULT_CONFIG_FILENAME.equals(filename)) {
				
				return null; // JSON config not present
				
			} else if ("/WEB-INF/jwkSet.json".equals(filename)) {
				return new ByteArrayInputStream(FILE_BASED_JWK_SET.toString(false).getBytes(StandardCharsets.UTF_8));
			} else {
				
				return null;
			}
		};
		
		JWKSet jwkSet = JWKSetLoader.loadWithDefaultSystemPropertyOverrideAndPKCS11Support(fs);
		
		assertEquals("1", jwkSet.getKeys().get(0).getKeyID());
		assertEquals("2", jwkSet.getKeys().get(1).getKeyID());
		assertEquals("hmac", jwkSet.getKeys().get(2).getKeyID());
		assertEquals("subject-encrypt", jwkSet.getKeys().get(3).getKeyID());
		assertEquals(4, jwkSet.getKeys().size());
	}
	
	
	@Test
	public void loadWithSysPropOverrideAndHSM_jwkSetFile_parseException() {
		
		FileInputStreamSource fs = filename -> {
			
			if (PKCS11Configuration.DEFAULT_CONFIG_FILENAME.equals(filename)) {
				
				return null; // JSON config not present
				
			} else if ("/WEB-INF/jwkSet.json".equals(filename)) {
				return new ByteArrayInputStream("invalid-key".getBytes(StandardCharsets.UTF_8));
			} else {
				
				return null;
			}
		};
		
		try {
			JWKSetLoader.loadWithDefaultSystemPropertyOverrideAndPKCS11Support(fs);
			fail();
		} catch (RuntimeException e) {
			assertEquals("Couldn't load JWK set file: /WEB-INF/jwkSet.json: Invalid JSON object", e.getMessage());
		}
	}
	
	
	@Test
	public void loadWithSysPropOverrideAndHSM_jwkSetFile_hsmConfigPresent() {
		
		FileInputStreamSource fs = filename -> {
			if (PKCS11Configuration.DEFAULT_CONFIG_FILENAME.equals(filename)) {
				
				return new ByteArrayInputStream((
						"pkcs11.enable = false\n" +
						"pkcs11.configFile = /WEB-INF/hsm.cfg\n" +
						"pkcs11.password = secret\n")
					.getBytes(StandardCharsets.UTF_8));
				
			} else if ("/WEB-INF/jwkSet.json".equals(filename)) {
				return new ByteArrayInputStream(FILE_BASED_JWK_SET
					.toString(false)
					.getBytes(StandardCharsets.UTF_8));
			} else {
				
				return null;
			}
		};
		
		JWKSet jwkSet = JWKSetLoader.loadWithDefaultSystemPropertyOverrideAndPKCS11Support(fs);
		
		assertEquals("1", jwkSet.getKeys().get(0).getKeyID());
		assertEquals("2", jwkSet.getKeys().get(1).getKeyID());
		assertEquals("hmac", jwkSet.getKeys().get(2).getKeyID());
		assertEquals("subject-encrypt", jwkSet.getKeys().get(3).getKeyID());
		assertEquals(4, jwkSet.getKeys().size());
	}
	
	
	@Test
	public void loadWithSysPropOverrideAndHSM_jwkSetFile_jwkSetFileNotFound() {
		
		FileInputStreamSource fs = filename -> {
			
			if (PKCS11Configuration.DEFAULT_CONFIG_FILENAME.equals(filename)) {
				
				return new ByteArrayInputStream((
						"pkcs11.enable = false\n" +
						"pkcs11.configFile = /WEB-INF/hsm.cfg\n" +
						"pkcs11.password = secret\n")
					.getBytes(StandardCharsets.UTF_8));
				
			} else if ("/WEB-INF/jwkSet.json".equals(filename)) {
				return null; // Not found
			} else {
				return null;
			}
		};
		
		JWKSet jwkSet = JWKSetLoader.loadWithDefaultSystemPropertyOverrideAndPKCS11Support(fs);
		
		assertEquals(0, jwkSet.getKeys().size());
	}
	
	
	@Test
	public void loadWithSysPropOverrideAndHSM_combinedJWKSet_emptyHSM() {
		
		FileInputStreamSource fs = filename -> {
			
			if (PKCS11Configuration.DEFAULT_CONFIG_FILENAME.equals(filename)) {
				
				return new ByteArrayInputStream((
						"pkcs11.enable = true\n" +
						"pkcs11.configFile = /WEB-INF/hsm.cfg\n" +
						"pkcs11.password = " + HSM_PIN + "\n")
					.getBytes(StandardCharsets.UTF_8));
				
			} else if ("/WEB-INF/jwkSet.json".equals(filename)) {
				return new ByteArrayInputStream(FILE_BASED_JWK_SET
					.toString(false)
					.getBytes(StandardCharsets.UTF_8));
				
			} else if ("/WEB-INF/hsm.cfg".equals(filename)) {
				return new ByteArrayInputStream(HSM_CONFIG
					.getBytes(StandardCharsets.UTF_8));
			} else {
				return null;
			}
		};
		
		JWKSet jwkSet = JWKSetLoader.loadWithDefaultSystemPropertyOverrideAndPKCS11Support(fs);
		
		assertEquals("1", jwkSet.getKeys().get(0).getKeyID());
		assertEquals("2", jwkSet.getKeys().get(1).getKeyID());
		assertEquals("hmac", jwkSet.getKeys().get(2).getKeyID());
		assertEquals("subject-encrypt", jwkSet.getKeys().get(3).getKeyID());
		assertEquals(4, jwkSet.getKeys().size());
	}
	
	
	@Test
	public void loadWithSysPropOverrideAndHSM_combinedJWKSet_hsmKey()
		throws Exception {
		
		String kidHSM = UUID.randomUUID().toString();
		
		generateRSAKeyWithSelfSignedCert(hsmKeyStore, kidHSM);
		
		FileInputStreamSource fs = filename -> {
			
			if (PKCS11Configuration.DEFAULT_CONFIG_FILENAME.equals(filename)) {
				
				return new ByteArrayInputStream((
						"pkcs11.enable = true\n" +
						"pkcs11.configFile = /WEB-INF/hsm.cfg\n" +
						"pkcs11.password = " + HSM_PIN + "\n")
					.getBytes(StandardCharsets.UTF_8));
				
			} else if ("/WEB-INF/jwkSet.json".equals(filename)) {
				return new ByteArrayInputStream(FILE_BASED_JWK_SET
					.toString(false)
					.getBytes(StandardCharsets.UTF_8));
				
			} else if ("/WEB-INF/hsm.cfg".equals(filename)) {
				return new ByteArrayInputStream(HSM_CONFIG
					.getBytes(StandardCharsets.UTF_8));
			} else {
				
				return null;
			}
		};
		
		JWKSet jwkSet = JWKSetLoader.loadWithDefaultSystemPropertyOverrideAndPKCS11Support(fs);
		
		assertEquals(1 + FILE_BASED_JWK_SET.size(), jwkSet.size());
		
		assertTrue(jwkSet.getKeys().get(0).isPrivate());
		assertEquals(kidHSM, jwkSet.getKeys().get(0).getKeyID());
		assertEquals(KeyUse.SIGNATURE, jwkSet.getKeys().get(0).getKeyUse());
		assertEquals("SunPKCS11-NitroKeyHSM", jwkSet.getKeys().get(0).getKeyStore().getProvider().getName());
		assertTrue(PKCS11Utils.isPKCS11Key(jwkSet.getKeys().get(0)));
		assertTrue(PKCS11Utils.hasPKCS11Key(jwkSet));
		
		assertTrue(jwkSet.getKeys().get(1).isPrivate());
		assertEquals("1", jwkSet.getKeys().get(1).getKeyID());
		
		assertTrue(jwkSet.getKeys().get(2).isPrivate());
		assertEquals("2", jwkSet.getKeys().get(2).getKeyID());
		
		assertEquals("hmac", jwkSet.getKeys().get(3).getKeyID());
		
		assertEquals("subject-encrypt", jwkSet.getKeys().get(4).getKeyID());
		
		assertEquals(5, jwkSet.getKeys().size());
		
		// Test RSA sign and verify
		var jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.RS256), new Payload("Hello, world!"));
		var rsaJWK = (RSAKey)jwkSet.getKeyByKeyId(kidHSM);
		JWSSigner signer = new RSASSASigner(rsaJWK.toPrivateKey());
		signer.getJCAContext().setProvider(rsaJWK.getKeyStore().getProvider());
		jwsObject.sign(signer);
		assertTrue(jwsObject.verify(new RSASSAVerifier((RSAKey) jwkSet.getKeyByKeyId(kidHSM))));
	}
	
	
	@Test
	public void loadWithSysPropOverrideAndHSM_jwkSet_hsmKeyWithSignatureUse()
		throws Exception {
		
		String kidHSM = UUID.randomUUID().toString();
		
		generateRSAKeyWithSelfSignedCert(hsmKeyStore, kidHSM, KeyUse.SIGNATURE);
		
		FileInputStreamSource fs = filename -> {
			
			if (PKCS11Configuration.DEFAULT_CONFIG_FILENAME.equals(filename)) {
				
				return new ByteArrayInputStream((
						"pkcs11.enable = true\n" +
						"pkcs11.configFile = /WEB-INF/hsm.cfg\n" +
						"pkcs11.password = " + HSM_PIN + "\n")
					.getBytes(StandardCharsets.UTF_8));
				
			} else if ("/WEB-INF/jwkSet.json".equals(filename)) {
				return new ByteArrayInputStream(FILE_BASED_JWK_SET
					.toString(false)
					.getBytes(StandardCharsets.UTF_8));
				
			} else if ("/WEB-INF/hsm.cfg".equals(filename)) {
				return new ByteArrayInputStream(HSM_CONFIG
					.getBytes(StandardCharsets.UTF_8));
			} else {
				
				return null;
			}
		};
		
		
		JWKSet jwkSet = JWKSetLoader.loadWithDefaultSystemPropertyOverrideAndPKCS11Support(fs);
		
		assertEquals(1 + FILE_BASED_JWK_SET.size(), jwkSet.size());
		
		var rsaJWK = (RSAKey) jwkSet.getKeyByKeyId(kidHSM);
		assertTrue(rsaJWK.isPrivate());
		assertEquals(kidHSM, rsaJWK.getKeyID());
		assertEquals(KeyUse.SIGNATURE, rsaJWK.getKeyUse());
		assertEquals("SunPKCS11-NitroKeyHSM", rsaJWK.getKeyStore().getProvider().getName());
		assertTrue(PKCS11Utils.isPKCS11Key(rsaJWK));
		assertTrue(PKCS11Utils.hasPKCS11Key(jwkSet));
		
		// Test RSA sign and verify
		var jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.RS256), new Payload("Hello, world!"));
		JWSSigner signer = new RSASSASigner(rsaJWK.toPrivateKey());
		signer.getJCAContext().setProvider(rsaJWK.getKeyStore().getProvider());
		jwsObject.sign(signer);
		assertTrue(jwsObject.verify(new RSASSAVerifier(rsaJWK)));
	}


	@Test
	public void loadWithSysPropOverrideAndHSM_jwkSet_hsmKeyWeak() {

		String kidHSM = UUID.randomUUID().toString();

		generateRSAKeyWithSelfSignedCert(hsmKeyStore, 1024, kidHSM, KeyUse.SIGNATURE);

		FileInputStreamSource fs = filename -> {

			if (PKCS11Configuration.DEFAULT_CONFIG_FILENAME.equals(filename)) {

				return new ByteArrayInputStream((
						"pkcs11.enable = true\n" +
						"pkcs11.configFile = /WEB-INF/hsm.cfg\n" +
						"pkcs11.password = " + HSM_PIN + "\n")
					.getBytes(StandardCharsets.UTF_8));

			} else if ("/WEB-INF/jwkSet.json".equals(filename)) {
				return new ByteArrayInputStream(FILE_BASED_JWK_SET
					.toString(false)
					.getBytes(StandardCharsets.UTF_8));

			} else if ("/WEB-INF/hsm.cfg".equals(filename)) {
				return new ByteArrayInputStream(HSM_CONFIG
					.getBytes(StandardCharsets.UTF_8));
			} else {

				return null;
			}
		};


		try {
			JWKSetLoader.loadWithDefaultSystemPropertyOverrideAndPKCS11Support(fs);
			fail();
		} catch (RuntimeException e) {
			System.out.println(e.getMessage());
			assertTrue(e.getMessage().startsWith("Found weak RSA key(s) shorter than 2048 bits with IDs: "));
		}
	}
	
	
	@Test
	public void loadWithSysPropOverrideAndHSM_jwkSet_hsmKeyWithSignatureUse_sysPropOverride()
		throws Exception {
		
		String kidHSM = UUID.randomUUID().toString();
		
		generateRSAKeyWithSelfSignedCert(hsmKeyStore, kidHSM, KeyUse.SIGNATURE);
		
		FileInputStreamSource fs = filename -> null;
		
		for (boolean base64EncodeConfig: List.of(false, true)) {
			
			System.setProperty("pkcs11.enable", "true");
			System.setProperty("pkcs11.configFile", base64EncodeConfig ? Base64.encode(HSM_CONFIG).toString() : HSM_CONFIG);
			System.setProperty("pkcs11.password", HSM_PIN);
			
			JWKSet jwkSet = JWKSetLoader.loadWithDefaultSystemPropertyOverrideAndPKCS11Support(fs);
			
			assertEquals(1, jwkSet.size());
			
			var rsaJWK = (RSAKey) jwkSet.getKeyByKeyId(kidHSM);
			assertTrue(rsaJWK.isPrivate());
			assertEquals(kidHSM, rsaJWK.getKeyID());
			assertEquals(KeyUse.SIGNATURE, rsaJWK.getKeyUse());
			assertEquals("SunPKCS11-NitroKeyHSM", rsaJWK.getKeyStore().getProvider().getName());
			assertTrue(PKCS11Utils.isPKCS11Key(rsaJWK));
			assertTrue(PKCS11Utils.hasPKCS11Key(jwkSet));
			
			// Test RSA sign and verify
			var jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.RS256), new Payload("Hello, world!"));
			JWSSigner signer = new RSASSASigner(rsaJWK.toPrivateKey());
			signer.getJCAContext().setProvider(rsaJWK.getKeyStore().getProvider());
			jwsObject.sign(signer);
			assertTrue(jwsObject.verify(new RSASSAVerifier(rsaJWK)));
		}
	}


	@Test
	public void loadWithSysPropOverrideAndHSM_jwkSet_hsmKeyWithSignatureUse_keyIDs_sysPropOverride()
		throws Exception {

		String kidHSM_1 = UUID.randomUUID().toString();
		String kidHSM_2 = UUID.randomUUID().toString();
		String kidHSM_3 = UUID.randomUUID().toString();

		generateRSAKeyWithSelfSignedCert(hsmKeyStore, kidHSM_1, KeyUse.SIGNATURE);
		generateRSAKeyWithSelfSignedCert(hsmKeyStore, kidHSM_2, KeyUse.SIGNATURE);
		generateRSAKeyWithSelfSignedCert(hsmKeyStore, kidHSM_3, KeyUse.SIGNATURE);

		FileInputStreamSource fs = filename -> null;

		for (boolean base64EncodeConfig: List.of(false, true)) {

			System.setProperty("pkcs11.enable", "true");
			System.setProperty("pkcs11.configFile", base64EncodeConfig ? Base64.encode(HSM_CONFIG).toString() : HSM_CONFIG);
			System.setProperty("pkcs11.password", HSM_PIN);
			System.setProperty("pkcs11.keyIDs.2", kidHSM_2);
			System.setProperty("pkcs11.keyIDs.3", kidHSM_3);

			JWKSet jwkSet = JWKSetLoader.loadWithDefaultSystemPropertyOverrideAndPKCS11Support(fs);

			assertEquals(2, jwkSet.size());

			for (String kid: List.of(kidHSM_2, kidHSM_3)) {

				// Get RSA key
				var rsaJWK = (RSAKey) jwkSet.getKeyByKeyId(kid);
				assertTrue(rsaJWK.isPrivate());
				assertEquals(kid, rsaJWK.getKeyID());
				assertEquals(KeyUse.SIGNATURE, rsaJWK.getKeyUse());
				assertEquals("SunPKCS11-NitroKeyHSM", rsaJWK.getKeyStore().getProvider().getName());
				assertTrue(PKCS11Utils.isPKCS11Key(rsaJWK));
				assertTrue(PKCS11Utils.hasPKCS11Key(jwkSet));

				// Test RSA sign and verify
				var jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.RS256), new Payload("Hello, world!"));
				JWSSigner signer = new RSASSASigner(rsaJWK.toPrivateKey());
				signer.getJCAContext().setProvider(rsaJWK.getKeyStore().getProvider());
				jwsObject.sign(signer);
				assertTrue(jwsObject.verify(new RSASSAVerifier(rsaJWK)));
			}
		}
	}
	
	
	@Test
	public void loadWithSysPropOverrideAndHSM_jwkSet_hsmKeyWithEncryptionUse_noKeyUseError()
		throws Exception {
		
		String kidHSM = UUID.randomUUID().toString();
		
		generateRSAKeyWithSelfSignedCert(hsmKeyStore, kidHSM, KeyUse.ENCRYPTION);
		
		FileInputStreamSource fs = filename -> {
			
			if (PKCS11Configuration.DEFAULT_CONFIG_FILENAME.equals(filename)) {
				
				return new ByteArrayInputStream((
						"pkcs11.enable = true\n" +
						"pkcs11.configFile = /WEB-INF/hsm.cfg\n" +
						"pkcs11.password = " + HSM_PIN + "\n")
					.getBytes(StandardCharsets.UTF_8));
				
			} else if ("/WEB-INF/jwkSet.json".equals(filename)) {
				return new ByteArrayInputStream(FILE_BASED_JWK_SET
					.toString(false)
					.getBytes(StandardCharsets.UTF_8));
				
			} else if ("/WEB-INF/hsm.cfg".equals(filename)) {
				return new ByteArrayInputStream(HSM_CONFIG
					.getBytes(StandardCharsets.UTF_8));
			} else {
				
				return null;
			}
		};
		
		
		JWKSet jwkSet = JWKSetLoader.loadWithDefaultSystemPropertyOverrideAndPKCS11Support(fs);
		
		assertEquals(1 + FILE_BASED_JWK_SET.size(), jwkSet.size());
		
		var rsaJWK = (RSAKey) jwkSet.getKeyByKeyId(kidHSM);
		assertTrue(rsaJWK.isPrivate());
		assertEquals(kidHSM, rsaJWK.getKeyID());
		assertEquals(KeyUse.ENCRYPTION, rsaJWK.getKeyUse());
		assertEquals("SunPKCS11-NitroKeyHSM", rsaJWK.getKeyStore().getProvider().getName());
		assertTrue(PKCS11Utils.isPKCS11Key(rsaJWK));
		assertTrue(PKCS11Utils.hasPKCS11Key(jwkSet));
		
		// Test RSA sign and verify
		var jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.RS256), new Payload("Hello, world!"));
		JWSSigner signer = new RSASSASigner(rsaJWK.toPrivateKey());
		signer.getJCAContext().setProvider(rsaJWK.getKeyStore().getProvider());
		jwsObject.sign(signer);
		assertTrue(jwsObject.verify(new RSASSAVerifier(rsaJWK)));
	}
	
	
	@Test
	public void loadWithSysPropOverrideAndHSM_jwkSet_hsmKeyUseDefaultsToSignature()
		throws Exception {
		
		String kidHSM = UUID.randomUUID().toString();
		
		generateRSAKeyWithSelfSignedCert(hsmKeyStore, kidHSM, null);
		
		FileInputStreamSource fs = filename -> {
			
			if (PKCS11Configuration.DEFAULT_CONFIG_FILENAME.equals(filename)) {
				
				return new ByteArrayInputStream((
						"pkcs11.enable = true\n" +
						"pkcs11.configFile = /WEB-INF/hsm.cfg\n" +
						"pkcs11.password = " + HSM_PIN + "\n")
					.getBytes(StandardCharsets.UTF_8));
				
			} else if ("/WEB-INF/jwkSet.json".equals(filename)) {
				return new ByteArrayInputStream(FILE_BASED_JWK_SET
					.toString(false)
					.getBytes(StandardCharsets.UTF_8));
				
			} else if ("/WEB-INF/hsm.cfg".equals(filename)) {
				return new ByteArrayInputStream(HSM_CONFIG
					.getBytes(StandardCharsets.UTF_8));
			} else {
				
				return null;
			}
		};
		
		
		JWKSet jwkSet = JWKSetLoader.loadWithDefaultSystemPropertyOverrideAndPKCS11Support(fs);
		
		assertEquals(1 + FILE_BASED_JWK_SET.size(), jwkSet.size());
		
		var rsaJWK = (RSAKey) jwkSet.getKeyByKeyId(kidHSM);
		assertTrue(rsaJWK.isPrivate());
		assertEquals(kidHSM, rsaJWK.getKeyID());
		assertEquals(KeyUse.SIGNATURE, rsaJWK.getKeyUse());
		assertEquals("SunPKCS11-NitroKeyHSM", rsaJWK.getKeyStore().getProvider().getName());
		assertTrue(PKCS11Utils.isPKCS11Key(rsaJWK));
		assertTrue(PKCS11Utils.hasPKCS11Key(jwkSet));
		
		// Test RSA sign and verify
		var jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.RS256), new Payload("Hello, world!"));
		JWSSigner signer = new RSASSASigner(rsaJWK.toPrivateKey());
		signer.getJCAContext().setProvider(rsaJWK.getKeyStore().getProvider());
		jwsObject.sign(signer);
		assertTrue(jwsObject.verify(new RSASSAVerifier(rsaJWK)));
	}
}
