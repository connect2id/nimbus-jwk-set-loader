package com.nimbusds.jose.jwk.loader;


import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import org.junit.Test;

import java.security.KeyPairGenerator;
import java.security.interfaces.RSAPublicKey;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;


public class JWKSetMergeTest {
	
	
	private static JWK generateRSAKey(final String kid)
		throws Exception {
		
		KeyPairGenerator gen = KeyPairGenerator.getInstance("RSA");
		gen.initialize(1024);
		return new RSAKey.Builder((RSAPublicKey) gen.generateKeyPair()
			.getPublic())
			.keyID(kid).build();
	}
	

	@Test
	public void testMergeWithNullPKCS11Set()
		throws Exception {
		
		JWKSet filedBasedJWKSet = new JWKSet(Arrays.asList(
			generateRSAKey("1"),
			generateRSAKey("2")));
		
		assertEquals(2, filedBasedJWKSet.getKeys().size());
		
		JWKSet out = JWKSetMerge.merge(filedBasedJWKSet, null);
		
		assertEquals("1", out.getKeys().get(0).getKeyID());
		assertEquals("2", out.getKeys().get(1).getKeyID());
		assertEquals(2, out.getKeys().size());
	}
	
	
	@Test
	public void testNullFileBasedJWKSet() {
		
		try {
			JWKSetMerge.merge(null, null);
			fail();
		} catch (NullPointerException e) {
			// ok
		}
	}
	
	
	@Test
	public void testMergeWithPKCS11Set()
		throws Exception {
		
		JWKSet filedBasedJWKSet = new JWKSet(Arrays.asList(
			generateRSAKey("1"),
			generateRSAKey("2")));
		
		// Simulate PKCS#11 key set
		JWKSet pkcs11Set = new JWKSet(Arrays.asList(
			generateRSAKey("pkcs-1"),
			generateRSAKey("pkcs-2")
		));
		
		JWKSet out = JWKSetMerge.merge(filedBasedJWKSet, pkcs11Set);
		
		assertEquals("pkcs-1", out.getKeys().get(0).getKeyID());
		assertEquals("pkcs-2", out.getKeys().get(1).getKeyID());
		assertEquals("1", out.getKeys().get(2).getKeyID());
		assertEquals("2", out.getKeys().get(3).getKeyID());
		assertEquals(4, out.getKeys().size());
	}
}
