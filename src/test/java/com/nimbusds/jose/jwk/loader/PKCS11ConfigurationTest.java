package com.nimbusds.jose.jwk.loader;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Test;


public class PKCS11ConfigurationTest {


	@After
	public void tearDown() {

		System.getProperties().remove("pkcs11.configFile");
		System.getProperties().remove("pkcs11.password");
		System.getProperties().remove("keyStore.pkcs11.op.configFile");
		System.getProperties().remove("keyStore.pkcs11.op.password");
	}
	
	
	@Test
	public void constants() {
		
		assertEquals("/WEB-INF/jose.properties", PKCS11Configuration.DEFAULT_CONFIG_FILENAME);
		assertEquals("pkcs11.", PKCS11Configuration.DEFAULT_CONFIG_PROPERTY_PREFIX);
	}
	

	@Test
	public void testDefault() {
		
		var config = new PKCS11Configuration();
		assertFalse(config.isPKCS11Enabled());
		assertNull(config.getPKCS11ConfigurationFile());
		assertFalse(config.hasPKCS11KeyStorePassword());
		assertEquals(0, config.getPKCS11KeyStorePassword().length);
		assertTrue(config.getPKCS11KeyIDs().isEmpty());

		config.log(Loggers.MAIN_LOG);
	}
	
	
	@Test
	public void testWithProperties_PKCS11Disabled() {
		
		var props = new Properties();
		props.setProperty("pkcs11.enable", "false");
		
		var config = new PKCS11Configuration(props);
		assertFalse(config.isPKCS11Enabled());
		assertNull(config.getPKCS11ConfigurationFile());
		assertFalse(config.hasPKCS11KeyStorePassword());
		assertEquals(0, config.getPKCS11KeyStorePassword().length);
		assertTrue(config.getPKCS11KeyIDs().isEmpty());

		config.log(Loggers.MAIN_LOG);
	}
	
	
	@Test
	public void testWithProperties_PKCS11Enabled() {
		
		var props = new Properties();
		props.setProperty("pkcs11.enable", "true");
		props.setProperty("pkcs11.configFile", "/WEB-INF/pkcs11.cfg");
		
		var config = new PKCS11Configuration(props);
		assertTrue(config.isPKCS11Enabled());
		assertEquals("/WEB-INF/pkcs11.cfg", config.getPKCS11ConfigurationFile());
		assertFalse(config.hasPKCS11KeyStorePassword());
		assertEquals(0, config.getPKCS11KeyStorePassword().length);
		assertTrue(config.getPKCS11KeyIDs().isEmpty());

		config.log(Loggers.MAIN_LOG);
	}


	@Test
	public void testWithProperties_PKCS11Enabled_keyIDs() {

		var props = new Properties();
		props.setProperty("pkcs11.enable", "true");
		props.setProperty("pkcs11.configFile", "/WEB-INF/pkcs11.cfg");
		props.setProperty("pkcs11.keyIDs.1", "c3c1f1ad-dfc8-4c76-96ce-2f31564d6ebd");
		props.setProperty("pkcs11.keyIDs.2", "be196a4a-5a7f-4af1-8e4e-b45517339cf0");
		props.setProperty("pkcs11.keyIDs.3", "86caefbe-e192-4628-8e6a-cc2380f50898");

		var config = new PKCS11Configuration(props);
		assertTrue(config.isPKCS11Enabled());
		assertEquals("/WEB-INF/pkcs11.cfg", config.getPKCS11ConfigurationFile());
		assertFalse(config.hasPKCS11KeyStorePassword());
		assertEquals(0, config.getPKCS11KeyStorePassword().length);

		assertEquals("c3c1f1ad-dfc8-4c76-96ce-2f31564d6ebd", config.getPKCS11KeyIDs().get(0));
		assertEquals("be196a4a-5a7f-4af1-8e4e-b45517339cf0", config.getPKCS11KeyIDs().get(1));
		assertEquals("86caefbe-e192-4628-8e6a-cc2380f50898", config.getPKCS11KeyIDs().get(2));

		assertEquals(3, config.getPKCS11KeyIDs().size());

		config.log(Loggers.MAIN_LOG);
	}


	@Test
	public void testWithProperties_PKCS11Enabled_keyIDs_empty() {

		var props = new Properties();
		props.setProperty("pkcs11.enable", "true");
		props.setProperty("pkcs11.configFile", "/WEB-INF/pkcs11.cfg");
		props.setProperty("pkcs11.keyIDs.1", "");
		props.setProperty("pkcs11.keyIDs.2", "");
		props.setProperty("pkcs11.keyIDs.3", "");

		var config = new PKCS11Configuration(props);
		assertTrue(config.isPKCS11Enabled());
		assertEquals("/WEB-INF/pkcs11.cfg", config.getPKCS11ConfigurationFile());
		assertFalse(config.hasPKCS11KeyStorePassword());
		assertEquals(0, config.getPKCS11KeyStorePassword().length);

		assertTrue(config.getPKCS11KeyIDs().isEmpty());

		config.log(Loggers.MAIN_LOG);
	}
	
	
	@Test
	public void testWithProperties_PKCS11Enabled_withPassword() {
		
		var props = new Properties();
		props.setProperty("pkcs11.enable", "true");
		props.setProperty("pkcs11.configFile", "/WEB-INF/pkcs11.cfg");
		props.setProperty("pkcs11.password", "1234");
		
		var config = new PKCS11Configuration(props);
		assertTrue(config.isPKCS11Enabled());
		assertEquals("/WEB-INF/pkcs11.cfg", config.getPKCS11ConfigurationFile());
		assertTrue(config.hasPKCS11KeyStorePassword());
		assertEquals("1234", new String(config.getPKCS11KeyStorePassword()));
		assertTrue(config.getPKCS11KeyIDs().isEmpty());

		config.log(Loggers.MAIN_LOG);
	}
	
	
	@Test
	public void testWithProperties_PKCS11Enabled_withPassword_trimWhitespace() {
		
		var props = new Properties();
		props.setProperty("pkcs11.enable", "true");
		props.setProperty("pkcs11.configFile", "/WEB-INF/pkcs11.cfg");
		props.setProperty("pkcs11.password", " 1234 ");
		
		var config = new PKCS11Configuration(props);
		assertTrue(config.isPKCS11Enabled());
		assertEquals("/WEB-INF/pkcs11.cfg", config.getPKCS11ConfigurationFile());
		assertTrue(config.hasPKCS11KeyStorePassword());
		assertEquals("1234", new String(config.getPKCS11KeyStorePassword()));
		assertTrue(config.getPKCS11KeyIDs().isEmpty());

		config.log(Loggers.MAIN_LOG);
	}
	
	
	@Test
	public void testWithProperties_missingPKCS11ConfigFile() {
		
		var props = new Properties();
		props.setProperty("pkcs11.enable", "true");
		
		try {
			new PKCS11Configuration(props);
			fail();
		} catch (RuntimeException e) {
			assertEquals("PKCS#11 is enabled by pkcs11.enable, but no PKCS#11 configuration is specified in pkcs11.configFile", e.getMessage());
		}
	}


	@Test
	public void testWithProperties_missingPKCS11ConfigFile_customPrefix() {

		var props = new Properties();
		props.setProperty("keyStore.pkcs11.op.enable", "true");

		try {
			new PKCS11Configuration("keyStore.pkcs11.op.", props, true);
			fail();
		} catch (RuntimeException e) {
			assertEquals("PKCS#11 is enabled by pkcs11.enable, but no PKCS#11 configuration is specified in keyStore.pkcs11.op.configFile", e.getMessage());
		}
	}
	
	
	@Test
	public void testLogNull() {
		
		new PKCS11Configuration().log(null);
	}
	
	
	@Test
	public void testLoadFromServletContext_null()
		throws IOException {
		
		PKCS11Configuration config = PKCS11Configuration.load(filename -> null);
		
		assertFalse(config.isPKCS11Enabled());

		config.log(Loggers.MAIN_LOG);
	}
	
	
	@Test
	public void testLoadFromServletContext_pkcs11Configured()
		throws IOException {
		
		FileInputStreamSource fs = filename -> {
			
			if (PKCS11Configuration.DEFAULT_CONFIG_FILENAME.equals(filename)) {
				
				return new ByteArrayInputStream(
					("pkcs11.enable = true\n" +
						"pkcs11.configFile = hsm.cfg\n" +
						"pkcs11.password = secret\n").getBytes(StandardCharsets.UTF_8));
				
			} else {
				
				return null;
			}
		};
		
		PKCS11Configuration config = PKCS11Configuration.load(fs);
		
		assertTrue(config.isPKCS11Enabled());
		assertEquals("hsm.cfg", config.getPKCS11ConfigurationFile());
		assertTrue(config.hasPKCS11KeyStorePassword());
		assertEquals("secret", new String(config.getPKCS11KeyStorePassword()));
		assertTrue(config.getPKCS11KeyIDs().isEmpty());

		config.log(Loggers.MAIN_LOG);
	}
	
	
	@Test
	public void testLoadFromServletContext_pkcs11Configured_sysPropertyOverride()
		throws IOException {
		
		System.getProperties().put("pkcs11.password", "othersecret");
		
		FileInputStreamSource fs = filename -> {
			
			if (PKCS11Configuration.DEFAULT_CONFIG_FILENAME.equals(filename)) {
				
				return new ByteArrayInputStream(
					("pkcs11.enable = true\n" +
						"pkcs11.configFile = hsm.cfg\n" +
						"pkcs11.password = secret\n").getBytes(StandardCharsets.UTF_8));
				
			} else {
				
				return null;
			}
		};
		
		PKCS11Configuration config = PKCS11Configuration.load(fs);
		
		assertTrue(config.isPKCS11Enabled());
		assertEquals("hsm.cfg", config.getPKCS11ConfigurationFile());
		assertTrue(config.hasPKCS11KeyStorePassword());
		assertEquals("othersecret", new String(config.getPKCS11KeyStorePassword()));
		assertTrue(config.getPKCS11KeyIDs().isEmpty());

		config.log(Loggers.MAIN_LOG);
	}
	
	
	@Test
	public void testSystemPropertiesOverrideDisabled() {
		
		var props = new Properties();
		props.put("pkcs11.enable", "true");
		props.put("pkcs11.configFile", "hsm.cfg");
		props.put("pkcs11.password", "secret");
		
		System.getProperties().put("pkcs11.password", "othersecret");
		
		PKCS11Configuration config = new PKCS11Configuration(props, false);
		assertTrue(config.isPKCS11Enabled());
		assertEquals("hsm.cfg", config.getPKCS11ConfigurationFile());
		assertArrayEquals("secret".toCharArray(), config.getPKCS11KeyStorePassword());
		assertTrue(config.getPKCS11KeyIDs().isEmpty());

		config.log(Loggers.MAIN_LOG);
	}
	
	
	@Test
	public void testSystemPropertiesOverrideEnabled() {
		
		var props = new Properties();
		props.put("pkcs11.enable", "true");
		props.put("pkcs11.configFile", "hsm.cfg");
		props.put("pkcs11.password", "secret");
		
		System.getProperties().put("pkcs11.configFile", "my-hsm.cfg");
		System.getProperties().put("pkcs11.password", "othersecret");
		
		PKCS11Configuration config = new PKCS11Configuration(props, true);
		assertTrue(config.isPKCS11Enabled());
		assertEquals("my-hsm.cfg", config.getPKCS11ConfigurationFile());
		assertArrayEquals("othersecret".toCharArray(), config.getPKCS11KeyStorePassword());
		assertTrue(config.getPKCS11KeyIDs().isEmpty());

		config.log(Loggers.MAIN_LOG);
	}


	@Test
	public void testSystemPropertiesOverrideEnabled_customPrefix() {

		var props = new Properties();
		props.put("keyStore.pkcs11.op.enable", "true");
		props.put("keyStore.pkcs11.op.configFile", "hsm.cfg");
		props.put("keyStore.pkcs11.op.password", "secret");

		System.getProperties().put("keyStore.pkcs11.op.configFile", "my-hsm.cfg");
		System.getProperties().put("keyStore.pkcs11.op.password", "othersecret");

		PKCS11Configuration config = new PKCS11Configuration("keyStore.pkcs11.op.", props, true);
		assertTrue(config.isPKCS11Enabled());
		assertEquals("my-hsm.cfg", config.getPKCS11ConfigurationFile());
		assertArrayEquals("othersecret".toCharArray(), config.getPKCS11KeyStorePassword());
		assertTrue(config.getPKCS11KeyIDs().isEmpty());

		config.log(Loggers.MAIN_LOG);
	}
}
