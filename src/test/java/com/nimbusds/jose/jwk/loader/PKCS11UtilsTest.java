package com.nimbusds.jose.jwk.loader;


import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.util.X509CertUtils;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.junit.Test;


public class PKCS11UtilsTest {
	

	@Test
	public void testIsPKCS11Key()
		throws Exception {
		
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(2048);
		KeyPair keyPair = keyPairGenerator.generateKeyPair();
		
		var issuer = new X500Name("cn=c2id");
		var serialNumber = new BigInteger(64, new SecureRandom());
		var now = new Date();
		var nbf = new Date(now.getTime() - 1000L);
		var exp = new Date(now.getTime() + 365 * 24 * 60 * 60 * 1000L); // in 1 year
		var subject = new X500Name("cn=c2id");
		var x509certBuilder = new JcaX509v3CertificateBuilder(
			issuer,
			serialNumber,
			nbf,
			exp,
			subject,
			keyPair.getPublic()
		);
		
		var signerBuilder = new JcaContentSignerBuilder("SHA256withRSA");
		X509CertificateHolder certHolder = x509certBuilder.build(signerBuilder.build(keyPair.getPrivate()));
		X509Certificate cert = X509CertUtils.parse(certHolder.getEncoded());
		assertNotNull(cert);
		
		KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
		keyStore.load(null, "".toCharArray());
		keyStore.setKeyEntry("1", keyPair.getPrivate(), "".toCharArray(), new java.security.cert.Certificate[]{cert});
		
		JWKSet jwkSet = JWKSet.load(keyStore, null);
		assertEquals(1, jwkSet.getKeys().size());
		
		var rsaKey = (RSAKey) jwkSet.getKeyByKeyId("1");
		assertNotNull(rsaKey.getX509CertChain());
		
		assertEquals("SUN", rsaKey.getKeyStore().getProvider().getName());
		
		assertFalse(PKCS11Utils.isPKCS11Key(rsaKey));
	}
}
