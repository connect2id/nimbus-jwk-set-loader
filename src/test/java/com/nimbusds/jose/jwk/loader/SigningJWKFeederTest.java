package com.nimbusds.jose.jwk.loader;


import java.math.BigInteger;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static org.junit.Assert.*;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.junit.Test;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import com.nimbusds.jose.util.Base64;
import com.nimbusds.jose.util.X509CertUtils;


public class SigningJWKFeederTest {
	
	
	static final Instant NOW = Instant.now();
	
	
	static RSAKey generatePlainKey(final String kid)
		throws JOSEException {
		
		return new RSAKeyGenerator(2048)
			.keyUse(KeyUse.SIGNATURE)
			.keyID(kid)
			.generate();
	}
	
	
	static RSAKey generateKeyWithCertificate(final String kid, final Instant nbf, final Instant exp)
		throws Exception {
		
		RSAKey plainJWK = generatePlainKey(kid);
		
		var issuer = new X500Name("cn=c2id");
		var serialNumber = new BigInteger(64, new SecureRandom());
		var subject = new X500Name("cn=c2id");
		var x509certBuilder = new JcaX509v3CertificateBuilder(
			issuer,
			serialNumber,
			Date.from(nbf),
			Date.from(exp),
			subject,
			plainJWK.toPublicKey()
		);
		
		var signerBuilder = new JcaContentSignerBuilder("SHA256withRSA");
		X509CertificateHolder certHolder = x509certBuilder.build(signerBuilder.build(plainJWK.toPrivateKey()));
		X509Certificate cert = X509CertUtils.parse(certHolder.getEncoded());
		
		assertEquals(nbf.getEpochSecond(), cert.getNotBefore().toInstant().getEpochSecond());
		assertEquals(exp.getEpochSecond(), cert.getNotAfter().toInstant().getEpochSecond());
		
		return new RSAKey.Builder(plainJWK)
			.x509CertChain(List.of(Base64.encode(cert.getEncoded())))
			.build();
	}
	
	
	@Test
	public void oneActiveKeyWithCertificate()
		throws Exception {
		
		List<RSAKey> rsaKeys = new LinkedList<>();
		
		// Plain
		RSAKey key_1 = generatePlainKey("1");
		rsaKeys.add(key_1);
		
		// With cert - expired
		RSAKey key_2 = generateKeyWithCertificate("2",
			NOW.minus(2, ChronoUnit.HOURS),
			NOW.minus(1, ChronoUnit.HOURS));
		rsaKeys.add(key_2);
		
		// With cert - valid
		RSAKey key_3 = generateKeyWithCertificate("3",
			NOW.minus(1, ChronoUnit.HOURS),
			NOW.plus(1, ChronoUnit.HOURS));
		rsaKeys.add(key_3);
		
		// With cert - valid in future
		RSAKey key_4 = generateKeyWithCertificate("4",
			NOW.plus(1, ChronoUnit.HOURS),
			NOW.plus(2, ChronoUnit.HOURS));
		rsaKeys.add(key_4);
		
		assertEquals(4, rsaKeys.size());
		
		var jwkFeeder = new SigningJWKFeeder<>(rsaKeys);
		
		RSAKey chosenKey = jwkFeeder.getJWK();
		
		assertEquals("3", chosenKey.getKeyID());
		
		assertEquals(4, jwkFeeder.size());
		
		assertEquals("1", jwkFeeder.plainKeys.get(0).getKeyID());
		assertEquals(1, jwkFeeder.plainKeys.size());
		
		List<String> sortedIDs = new LinkedList<>();
		jwkFeeder.sortedX509Keys.forEach((key, value) -> sortedIDs.add(value.getKeyID()));
		assertEquals(List.of("4", "3", "2"), sortedIDs);
	}
	
	
	@Test
	public void twoActiveKeysWithCertificate()
		throws Exception {
		
		List<RSAKey> rsaKeys = new LinkedList<>();
		
		// Plain
		RSAKey key_1 = generatePlainKey("1");
		rsaKeys.add(key_1);
		
		// With cert - expired
		RSAKey key_2 = generateKeyWithCertificate("2",
			NOW.minus(2, ChronoUnit.HOURS),
			NOW.minus(1, ChronoUnit.HOURS));
		rsaKeys.add(key_2);
		
		// With cert - valid
		RSAKey key_3 = generateKeyWithCertificate("3",
			NOW.minus(1, ChronoUnit.HOURS),
			NOW.plus(1, ChronoUnit.HOURS));
		rsaKeys.add(key_3);
		
		// With cert - valid, more recent
		RSAKey key_4 = generateKeyWithCertificate("4",
			NOW.minus(1, ChronoUnit.HOURS),
			NOW.plus(3, ChronoUnit.HOURS));
		rsaKeys.add(key_4);
		
		// With cert - valid in future
		RSAKey key_5 = generateKeyWithCertificate("5",
			NOW.plus(1, ChronoUnit.HOURS),
			NOW.plus(4, ChronoUnit.HOURS));
		rsaKeys.add(key_5);
		
		assertEquals(5, rsaKeys.size());
		
		var jwkFeeder = new SigningJWKFeeder<>(rsaKeys);
		
		RSAKey chosenKey = jwkFeeder.getJWK();
		
		assertEquals("4", chosenKey.getKeyID());
		
		assertEquals(5, jwkFeeder.size());
		
		assertEquals("1", jwkFeeder.plainKeys.get(0).getKeyID());
		assertEquals(1, jwkFeeder.plainKeys.size());
		
		List<String> sortedIDs = new LinkedList<>();
		jwkFeeder.sortedX509Keys.forEach((key, value) -> sortedIDs.add(value.getKeyID()));
		assertEquals(List.of("5", "4", "3", "2"), sortedIDs);
	}
	
	
	@Test
	public void noActiveKeyWithCertificate()
		throws Exception {
		
		List<RSAKey> rsaKeys = new LinkedList<>();
		
		// Plain
		RSAKey key_1 = generatePlainKey("1");
		rsaKeys.add(key_1);
		
		// With cert - expired
		RSAKey key_2 = generateKeyWithCertificate("2",
			NOW.minus(2, ChronoUnit.HOURS),
			NOW.minus(1, ChronoUnit.HOURS));
		rsaKeys.add(key_2);
		
		// With cert - valid in future
		RSAKey key_3 = generateKeyWithCertificate("4",
			NOW.plus(1, ChronoUnit.HOURS),
			NOW.plus(2, ChronoUnit.HOURS));
		rsaKeys.add(key_3);
		
		assertEquals(3, rsaKeys.size());
		
		SigningJWKFeeder<RSAKey> jwkFeeder = new SigningJWKFeeder<>(rsaKeys);
		
		RSAKey chosenKey = jwkFeeder.getJWK();
		assertEquals("4", chosenKey.getKeyID());
		
		assertEquals(3, jwkFeeder.size());
		
		assertEquals("1", jwkFeeder.plainKeys.get(0).getKeyID());
		assertEquals(1, jwkFeeder.plainKeys.size());
		
		List<String> sortedIDs = new LinkedList<>();
		jwkFeeder.sortedX509Keys.forEach((key, value) -> sortedIDs.add(value.getKeyID()));
		assertEquals(List.of("4", "2"), sortedIDs);
	}
	
	
	@Test
	public void onePlainKey()
		throws Exception {
		
		var jwkFeeder = new SigningJWKFeeder<>(Collections.singletonList(generatePlainKey("1")));
		
		RSAKey chosenKey = jwkFeeder.getJWK();
		assertEquals("1", chosenKey.getKeyID());
		
		assertEquals(1, jwkFeeder.size());
		
		assertEquals("1", jwkFeeder.plainKeys.get(0).getKeyID());
		assertEquals(1, jwkFeeder.plainKeys.size());
		
		assertTrue(jwkFeeder.sortedX509Keys.isEmpty());
	}
	
	
	@Test
	public void twoPlainKeys_preserveOrder()
		throws Exception {
		
		var jwkFeeder = new SigningJWKFeeder<>(Arrays.asList(
			generatePlainKey("1"),
			generatePlainKey("2")
		));
		
		RSAKey chosenKey = jwkFeeder.getJWK();
		assertEquals("1", chosenKey.getKeyID());
		
		assertEquals(2, jwkFeeder.size());
		
		assertEquals("1", jwkFeeder.plainKeys.get(0).getKeyID());
		assertEquals("2", jwkFeeder.plainKeys.get(1).getKeyID());
		assertEquals(2, jwkFeeder.plainKeys.size());
		
		assertTrue(jwkFeeder.sortedX509Keys.isEmpty());
	}
	
	
	@Test
	public void nullKeyListArgument() {
		
		try {
			new SigningJWKFeeder<RSAKey>(null);
			fail();
		} catch (NullPointerException e) {
			// ok
		}
	}
	
	
	@Test
	public void emptyKeyKeyArgument() {
		
		try {
			new SigningJWKFeeder<RSAKey>(Collections.emptyList());
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The JWK list must not be empty", e.getMessage());
		}
	}
	
	
	@Test
	public void rejectNonSignatureKeys()
		throws Exception {
		
		try {
			new SigningJWKFeeder<>(Arrays.asList(
				new RSAKey.Builder(generatePlainKey("1")).keyUse(null).build(),
				new RSAKey.Builder(generatePlainKey("2")).keyUse(null).build()));
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The use of JWK with ID 1 must be signature", e.getMessage());
		}
	}
	
	
	@Test
	public void rejectNonSignatureKeys_second()
		throws Exception {
		
		try {
			new SigningJWKFeeder<>(Arrays.asList(
				generatePlainKey("1"),
				new RSAKey.Builder(generatePlainKey("2")).keyUse(null).build()));
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The use of JWK with ID 2 must be signature", e.getMessage());
		}
	}


	@Test
	public void rejectPublicOnlySignatureKeys()
		throws Exception {

		try {
			new SigningJWKFeeder<>(Arrays.asList(
				new RSAKey.Builder(generatePlainKey("1")).keyUse(KeyUse.SIGNATURE).build(),
				new RSAKey.Builder(generatePlainKey("2")).keyUse(KeyUse.SIGNATURE).build().toPublicJWK()));
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The JWK with ID 2 has no private part", e.getMessage());
		}
	}
}
