package com.nimbusds.jose.jwk.loader;


import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;


public class WeakRSAKeyDetectorTest {
	
	
	@Test
	public void empty() throws JOSEException {
		
		assertTrue(WeakRSAKeyDetector.findWeakRSAKeys(new JWKSet()).isEmpty());

		WeakRSAKeyDetector.ensureNoWeakRSAKeys(new JWKSet());
	}
	
	
	@Test
	public void allPass() throws JOSEException {
		
		List<JWK> jwkList = new LinkedList<>();
		
		for (int i=0; i < 2; i++) {
			jwkList.add(new RSAKeyGenerator(2048)
				.keyID("key-" + i)
				.keyUse(KeyUse.SIGNATURE)
				.generate());
		}
		
		assertTrue(WeakRSAKeyDetector.findWeakRSAKeys(new JWKSet(jwkList)).isEmpty());

		WeakRSAKeyDetector.ensureNoWeakRSAKeys(new JWKSet(jwkList));
	}
	
	
	@Test
	public void allWeak() throws JOSEException {
		
		List<JWK> jwkList = new LinkedList<>();
		
		for (int i=0; i < 2; i++) {
			jwkList.add(new RSAKeyGenerator(1024, true)
				.keyID("key-" + i)
				.keyUse(KeyUse.SIGNATURE)
				.generate());
		}
		
		assertEquals(List.of("key-0", "key-1"), WeakRSAKeyDetector.findWeakRSAKeys(new JWKSet(jwkList)));

		try {
			WeakRSAKeyDetector.ensureNoWeakRSAKeys(new JWKSet(jwkList));
			fail();
		} catch (JOSEException e) {
			assertEquals("Found weak RSA key(s) shorter than 2048 bits with IDs: [key-0, key-1]", e.getMessage());
		}
	}
	
	
	@Test
	public void secondWeak() throws JOSEException {
		
		List<JWK> jwkList = Arrays.asList(
			new RSAKeyGenerator(2048)
				.keyID("key-0")
				.keyUse(KeyUse.SIGNATURE)
				.generate(),
			new RSAKeyGenerator(1024, true)
				.keyID("key-1")
				.keyUse(KeyUse.SIGNATURE)
				.generate());
		
		assertEquals(List.of("key-1"), WeakRSAKeyDetector.findWeakRSAKeys(new JWKSet(jwkList)));

		try {
			WeakRSAKeyDetector.ensureNoWeakRSAKeys(new JWKSet(jwkList));
			fail();
		} catch (JOSEException e) {
			assertEquals("Found weak RSA key(s) shorter than 2048 bits with IDs: [key-1]", e.getMessage());
		}
	}
	
	
	@Test
	public void secondWeak_nullKID() throws JOSEException {
		
		List<JWK> jwkList = Arrays.asList(
			new RSAKeyGenerator(2048)
				.keyID("key-0")
				.keyUse(KeyUse.SIGNATURE)
				.generate(),
			new RSAKeyGenerator(1024, true)
				.keyID(null)
				.keyUse(KeyUse.SIGNATURE)
				.generate());
		
		assertEquals(Collections.singletonList((String)null), WeakRSAKeyDetector.findWeakRSAKeys(new JWKSet(jwkList)));

		try {
			WeakRSAKeyDetector.ensureNoWeakRSAKeys(new JWKSet(jwkList));
			fail();
		} catch (JOSEException e) {
			assertEquals("Found weak RSA key(s) shorter than 2048 bits with IDs: [null]", e.getMessage());
		}
	}
}
