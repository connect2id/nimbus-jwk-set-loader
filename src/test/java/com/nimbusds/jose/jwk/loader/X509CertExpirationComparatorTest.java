package com.nimbusds.jose.jwk.loader;


import com.nimbusds.jose.jwk.JWK;
import org.junit.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.nimbusds.jose.jwk.loader.SigningJWKFeederTest.*;
import static org.junit.Assert.assertEquals;


public class X509CertExpirationComparatorTest {
	
	
	@Test
	public void comparatorNoCert()
		throws Exception {
		
		JWK jwk0 = generatePlainKey("0");
		JWK jwk1 = generatePlainKey("1");
		
		var certComparator = new X509CertExpirationComparator();
		
		assertEquals("a = b", 0, certComparator.compare(jwk0, jwk1));
	}
	
	
	@Test
	public void comparator_withCert_withoutCert()
		throws Exception {
		
		JWK jwk0 = generateKeyWithCertificate("0", NOW, NOW.plusSeconds(60));
		JWK jwk1 = generatePlainKey("1");
		
		var certComparator = new X509CertExpirationComparator();
		
		assertEquals("a > b", 1, certComparator.compare(jwk0, jwk1));
	}
	
	
	@Test
	public void comparatorEqual()
		throws Exception {
		
		Instant exp = NOW;
		
		JWK jwk0 = generateKeyWithCertificate("0", NOW, exp);
		JWK jwk1 = generateKeyWithCertificate("1", NOW, exp);
		
		var certComparator = new X509CertExpirationComparator();
		
		assertEquals("a = b", 0, certComparator.compare(jwk0, jwk1));
	}
	
	
	@Test
	public void comparatorLessThan()
		throws Exception {
		
		JWK jwk0 = generateKeyWithCertificate("0", NOW, NOW.minusSeconds(60));
		JWK jwk1 = generateKeyWithCertificate("1", NOW, NOW);
		
		var certComparator = new X509CertExpirationComparator();
		
		assertEquals("a < b", -1, certComparator.compare(jwk0, jwk1));
	}
	
	
	@Test
	public void comparatorGreaterThan()
		throws Exception {
		
		JWK jwk0 = generateKeyWithCertificate("0", NOW, NOW.plusSeconds(60));
		JWK jwk1 = generateKeyWithCertificate("1", NOW, NOW);
		
		var certComparator = new X509CertExpirationComparator();
		
		assertEquals("a > b", 1, certComparator.compare(jwk0, jwk1));
	}
	
	
	@Test
	public void sort_noChange()
		throws Exception {
		
		JWK jwk0 = generateKeyWithCertificate("0", NOW, NOW);
		JWK jwk1 = generateKeyWithCertificate("1", NOW, NOW.plus(1L, ChronoUnit.DAYS));
		JWK jwk2 = generateKeyWithCertificate("2", NOW, NOW.plus(2L, ChronoUnit.DAYS));
		JWK jwk3 = generateKeyWithCertificate("3", NOW, NOW.plus(3L, ChronoUnit.DAYS));
		
		List<JWK> list = Arrays.asList(jwk0, jwk1, jwk2, jwk3);
		
		var certComparator = new X509CertExpirationComparator();
		
		list.sort(certComparator);
		
		assertEquals("0", list.get(0).getKeyID());
		assertEquals("1", list.get(1).getKeyID());
		assertEquals("2", list.get(2).getKeyID());
		assertEquals("3", list.get(3).getKeyID());
	}
	
	
	@Test
	public void sort_reversed()
		throws Exception {
		
		JWK jwk0 = generateKeyWithCertificate("0", NOW, NOW);
		JWK jwk1 = generateKeyWithCertificate("1", NOW, NOW.plus(1L, ChronoUnit.DAYS));
		JWK jwk2 = generateKeyWithCertificate("2", NOW, NOW.plus(2L, ChronoUnit.DAYS));
		JWK jwk3 = generateKeyWithCertificate("3", NOW, NOW.plus(3L, ChronoUnit.DAYS));
		
		List<JWK> list = Arrays.asList(jwk3, jwk2, jwk1, jwk0); // reverse
		
		var certComparator = new X509CertExpirationComparator();
		
		list.sort(certComparator);
		
		assertEquals("0", list.get(0).getKeyID());
		assertEquals("1", list.get(1).getKeyID());
		assertEquals("2", list.get(2).getKeyID());
		assertEquals("3", list.get(3).getKeyID());
	}
	
	
	@Test
	public void sort_mixed()
		throws Exception {
		
		JWK jwk0 = generateKeyWithCertificate("0", NOW, NOW);
		JWK jwk1 = generateKeyWithCertificate("1", NOW, NOW.plus(1L, ChronoUnit.DAYS));
		JWK jwk2 = generateKeyWithCertificate("2", NOW, NOW.plus(2L, ChronoUnit.DAYS));
		JWK jwk3 = generateKeyWithCertificate("3", NOW, NOW.plus(3L, ChronoUnit.DAYS));
		
		List<JWK> list = Arrays.asList(jwk1, jwk3, jwk0, jwk2);
		
		var certComparator = new X509CertExpirationComparator();
		
		list.sort(certComparator);
		
		assertEquals("0", list.get(0).getKeyID());
		assertEquals("1", list.get(1).getKeyID());
		assertEquals("2", list.get(2).getKeyID());
		assertEquals("3", list.get(3).getKeyID());
		
		// Reverse
		list.sort(Collections.reverseOrder(certComparator));
		
		assertEquals("3", list.get(0).getKeyID());
		assertEquals("2", list.get(1).getKeyID());
		assertEquals("1", list.get(2).getKeyID());
		assertEquals("0", list.get(3).getKeyID());
	}
}
